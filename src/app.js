const path = require('path')
const express = require('express')
const hbs = require('hbs')
const geocode = require('./utils/geocode')
const forecast = require('./utils/forecast')

const app = express()
const port = process.env.PORT || 3000 ;

// Define paths for Express config
const publicDirectoryPath = path.join(__dirname, '../public')
const viewsPath = path.join(__dirname, '../templates/views')
const partialsPath = path.join(__dirname, '../templates/partials')

// Setup handlebars engine and views location
app.set('view engine', 'hbs')
app.set('views', viewsPath)
hbs.registerPartials(partialsPath)

// Setup static directory to serve
app.use(express.static(publicDirectoryPath))

app.get('', (req, res) => {
    res.render('index', {
        title: 'ta9s :)',
        name: 'mahmoud'
    })
})

app.get('/about', (req, res) => {
    res.render('about', {
        title: 'chkoun ?',
        name: 'mahmoud'
    })
})

app.get('/help', (req, res) => {
    res.render('help', {
        helpText: 'ma3andi fech n3awnek ',
        title: '3awenni',
        name: 'mahmoud'
    })
})

app.get('/weather', (req, res) => {
    if (!req.query.address) {
        return res.send({
            error: 'lazmek t7ot adresse s7i7a'
        })
    }

    geocode(req.query.address, (error, { latitude, longitude, location } = {}) => {
        if (error) {
            return res.send({ error })
        }

        forecast(latitude, longitude, (error, forecastData) => {
            if (error) {
                return res.send({ error })
            }
                
            res.send({
                forecast: forecastData,
                location,
                address: req.query.address
            })
        })
    })
})

app.get('/products', (req, res) => {
    if (!req.query.search) {
        return res.send({
            error: 'mouch lazem '
        })
    }

    console.log(req.query.search)
    res.send({
        products: []
    })
})

app.get('/help/*', (req, res) => {
    res.render('404', {
        title: '404',
        name: 'mahmoud',
        errorMessage: '.'
    })
})

app.get('*', (req, res) => {
    res.render('404', {
        title: '404',
        name: 'mahmoud',
        errorMessage: 'page mafamech'
    })
})

app.listen(port, () => {
    console.log('Server is up on port ' +port)
})