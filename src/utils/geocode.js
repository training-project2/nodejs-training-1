const request = require('request')

const geocode = (address, callback) => {
    const url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/' + address + '.json?access_token=pk.eyJ1IjoibWFobW91ZC1hbWluZSIsImEiOiJja29sd21obWQxamRvMnhybWcwOHA5ampsIn0.zn_WN3sBijoccB69YOnYqw&limit=1'

    request({ url, json: true }, (error, { body }) => {
        if (error) {
            callback('najamch nconnecti mapbox', undefined)
        } else if (body.features.length === 0) {
            callback('blasa mouch mawjouda , lawej haja okhra hhh ', undefined)
        } else {
            callback(undefined, {
                latitude: body.features[0].center[1],
                longitude: body.features[0].center[0],
                location: body.features[0].place_name
            })
        }
    })
}

module.exports = geocode