const request = require('request')

const forecast = (latitude, longitude, callback) => {
    const url = 'http://api.weatherstack.com/current?access_key=51b312739c1ab12db0f47b76f9ff4415&query=' + latitude + ',' + longitude

    request({ url, json: true }, (error, { body }) => {
        console.log(body)
        if (error) {
            callback('najamch nconnecti weatherstack', undefined)
        } else if (body.error) {
            callback('blassa mouch mawjouda', undefined)
        } else {
            callback(undefined, body.current.weather_descriptions + ' , temperature tawa  ' + body.current.temperature + '. famma  ' + body.current.precip + '% bech mtar tsob.')
        }
    })
}

module.exports = forecast